//console.log("Hello, B204!");

/*
	Objects
		-An object is a data type that is used to represent a real world object
		-It is a collection of related data and/or functionalities
		-Information is stored in object represented in "key: value" pair
			key -> property of the object
			value -> actual data to be stored
		- Different data types may be stored in an object's property creating complex data structures

		Two ways of creating object in javascript
			1. Object Literal Notation 
				let/const objectName = {}

			2. Object Constructor Notation
				Object Instantiation ( let object = new Object() )
	
	Object Literal Notation
		- This creates/declares an object and also initializes/assigns it's properties upon creation
		- A cellphone is an example of a real world object
		- It has it's own properties such as name, color, weight, unit model and a lot of other things

		Syntax:
			let/const = {
				keyA: value,
				keyB: valueB
			};
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using literal notation");
console.log(cellphone);

console.log(typeof cellphone);


let cellphone2 = {
	name: "Motorola",
	manufactureDate: 2000
};

console.log(cellphone2);


//Creating objects using constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
	 

	
	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters

function Laptop(name, manufactureDate) {

	//this.propertyName = value
	this.name = name;
	this.manufactureDate = manufactureDate;
}


// This is a unique instance of the Laptop object
/*
    - The "new" operator creates an instance of an object
    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/

let laptop = new Laptop('Lenovo', 2008);
console.log(laptop);

let oldLaptop = Laptop('IBM', 1980);
console.log(oldLaptop); //undefined

let myLaptop = new Laptop('MacBook', 2020);
console.log(myLaptop);


//Creating empty objects
let computer = {};
console.log(computer);

let myComputer = new Object();
console.log(myComputer);



//Accessing Object Properties

//objectName.propertyName

//Using the dot notation
console.log(myLaptop.name); // MacBook
console.log(myLaptop.manufactureDate); // 2020

//Using the square bracket notation
console.log(myLaptop['name']);


//Accessing array objects
//let array = [{name: "Lenovo", manufactureDate: 2008} , {name: "MacBook", manufactureDate: 2020}]
let array = [laptop, myLaptop]; //MacBook
console.log(array);

//arrayName[indexNumber].propertyName
//This tells us the array[1] is an object by using the dot
console.log(array[1].name); //MacBook

console.log(array[1]['name']);


//Initializing/Adding/Deleting/Reassigning Object Properties
let car = {};
console.log(car);

//objectName.propertyName = value
car.name = 'Sarao';
console.log("Result from adding properties using dot notation:");
console.log(car);

car['manufacture date'] = 2019;
console.log(car);

//Deleting object properties
delete car['manufacture date'];
console.log(car);

car.manufactureDate = 2019;
console.log(car);

//Reassigning object properties
car.name = "Mustang";
console.log(car);

//Object Methods
/*
	-A method is a function which is a property of an object
	-They are also functions and one of the key differences they have is that methods are functions related to a specific object
	-Methods are useful for creating object specific function which are used to perform tasks on them

*/

let person = {
	//property
	name: 'Jack',

	//method
	talk: function() {
		console.log('Hello! My name is ' + this.name);
	}
}
console.log(person);
person.talk();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.");
}

person.walk();

//Methods are useful for creating reusable functions that performs tasks related to object


let friend = {
	firstName: 'Rafael',
	lastName: 'Santillan',
	address: {
		city: 'Quezon City',
		country: 'Philippines'
	},
	email: ['raf@mail.com', 'raf123@yahoo.com'],

	introduce: function() {
		console.log('Hello! My name is ' +this.firstName + ' ' + this.lastName);
	}
}

friend.introduce();


//Real World Application of Objects
/*

	Scenario:
		1. We would like to create a game that would have several pokemon interact with each other
		2. Every pokemon would have the same set of stats, properties, and functions
*/

let myPokemon = {
	//properties
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,

	//methods
	tackle: function() {
		console.log("This Pokemon tackle target pokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},

	faint: function() {
		console.log("Pokemon fainted.");
	}

}

console.log(myPokemon);



function Pokemon(name, level, health) {
	//properties
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = level;

	/*
	snorlax.tackle({name: 'Squirtle', level: 99, health: 200, attack: 99});

	*/
	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

			//200 - 75 = 125
	},

	this.faint = function() {
		console.log(this.name + " fainted");
	}
}

/*
let snorlax = {
	name: 'Snorlax', 
	level: 75, 
	health: 500, 
	attack: 75, 
};

let squirtle = {
	name: 'Squirtle', 
	level: 99, 
	health: 200, 
	attack: 99
}
*/

let squirtle = new Pokemon("Squirtle", 99, 200);
let snorlax = new Pokemon("Snorlax", 75, 500);

console.log(squirtle);
console.log(snorlax);

//object.method(target)
snorlax.tackle(squirtle);
snorlax.tackle(squirtle);

/*

Mini-Activity: 20 mins.
1. Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/